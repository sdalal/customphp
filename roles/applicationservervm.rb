name "applicationservervm"
description "Application server Infrastructure VM setup & configuration"

all_env = [
	"recipe[applicationserver-setup]",
]

run_list(all_env)

env_run_list(
	"_default" => all_env,
	"dev"	   => all_env,
)
