name "batchservervm"
description "Batch server Infrastructure VM setup & configuration"

all_env = [
	"recipe[Batchserver-setup]",
]

run_list(all_env)

env_run_list(
	"_default" => all_env,
	"dev"	   => all_env,
)
