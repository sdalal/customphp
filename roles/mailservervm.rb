name "mailservervm"
description "Mailserver Infrastructure VM setup & configuration"

all_env = [
	"recipe[mailserver-setup]",
]

run_list(all_env)

env_run_list(
	"_default" => all_env,
	"dev"	   => all_env,
)
