name "webservervm"
description "Webserver Infrastructure VM setup & configuration"

all_env = [
	"recipe[webserver-setup]",
]

run_list(all_env)

env_run_list(
	"_default" => all_env,
	"dev"	   => all_env,
)
