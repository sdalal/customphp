name "SPECpoll"
description "SPECpoll configuration for all VM"

all_env = [
	"recipe[specpoll]",
]

run_list(all_env)

env_run_list(
	"_default" => all_env,
	"dev"	   => all_env,
)
