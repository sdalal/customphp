name "workloadvm"
description "Workload Infrastructure VM setup & configuration"

all_env = [
	"recipe[workload-setup]",
]

run_list(all_env)

env_run_list(
	"_default" => all_env,
	"dev"	   => all_env,
)
