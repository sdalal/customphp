name "java"
description "Install JDK 6"

all_env = [
	"recipe[java]",
]

run_list(all_env)

env_run_list(
	"_default" => all_env,
	"dev"	   => all_env,
)
