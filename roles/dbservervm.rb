name "dbservervm"
description "Database server Infrastructure VM setup & configuration"

all_env = [
	"recipe[dbserver-setup]",
]

run_list(all_env)

env_run_list(
	"_default" => all_env,
	"dev"	   => all_env,
)
