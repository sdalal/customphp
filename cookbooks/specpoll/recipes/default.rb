#
# Cookbook Name:: specpoll
# Recipe:: default
#
# Copyright 2013, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
apt_package "unzip" do
  action :install
end

script "install SPECpoll" do
  interpreter "bash"
  user "root"
  cwd "/opt"
  code <<-EOH
  wget https://s3.amazonaws.com/SPECpoll/SPECpoll.tar.gz
  tar -zxvf SPECpoll.tar.gz
  echo "java -jar /opt/SPECpoll/pollme.jar -p 8001" >> /etc/rc.local
  /usr/binjava -jar /opt/SPECpoll/pollme.jar -p 8001 &
  EOH
  not_if do FileTest.directory?('/opt/SPECpoll') end
  not_if do FileTest.file?('/opt/SPECpoll.tar.gz') end 
end
