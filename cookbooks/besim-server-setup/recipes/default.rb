#
# Cookbook Name:: specpoll
# Recipe:: default
#
# Copyright 2013, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
script "install besim-server-setup " do
  interpreter "bash"
  user "root"
  cwd "/opt"
  code <<-EOH
  wget https://s3.amazonaws.com/SPECpoll/SPECweb2005.tar.gz
  tar -zxvf SPECweb2005.tar.gz 
  EOH
  not_if do FileTest.directory?('/opt/SPECweb2005.tar.gz') end
  not_if do FileTest.file?('/opt/SPECweb2005.tar.gz') end 
end
